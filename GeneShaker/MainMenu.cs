﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xenko.Core.Mathematics;
using Xenko.Input;
using Xenko.Engine;
using Xenko.UI;
using Xenko.UI.Controls;
using Xenko.Games;
using Xenko.UI.Events;

namespace GeneShaker.MainMenu
{
    public class MainMenu : SyncScript
    {
        // Declared public member fields and properties will show in the game studio
        public Scene GameScene;
        public BasePair TestBasePair;

        public override void Start()
        {
            BindLambdaToButton("ButtonEnterGame", (o, i) =>
            {
                SceneSystem.SceneInstance.RootScene = GameScene;
            });

            BindLambdaToButton("ButtonQuitGame", (o, i) =>
            {
                (Game as GameBase).Exit();
            });
        }

        private void BindLambdaToButton(string buttonName, EventHandler<RoutedEventArgs> eventFuntion)
        {
            var button = (Button)Entity.Get<UIComponent>().Page.RootElement.FindName(buttonName);

            if (button == null)
                return;

            button.Click += eventFuntion;
        }

        public override void Update()
        {
        }
    }
}
