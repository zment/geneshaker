﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xenko.Core.Mathematics;
using Xenko.Input;
using Xenko.Engine;

namespace GeneShaker
{
    public class RandomXYMovement : SyncScript
    {
        public float ChangeInterval = 1;
        public float Speed = 1;
        public float MaxTargetDistance = 1;
        public float MaxVelocity = 1;

        private Vector3 startPosition;
        private Vector3 currentTarget;
        private TimeSpan timeStarted;

        private static Random random;
        private Vector3 velocity;

        private float randomDouble => (float) random.NextDouble() * MaxTargetDistance;
        private Vector3 randomVector
        {
            get
            {
                var randomVector = new Vector3(randomDouble, randomDouble, randomDouble);
                if (randomVector.Length() > MaxTargetDistance)
                {
                    randomVector = Vector3.Normalize(randomVector);
                    randomVector = Vector3.Multiply(randomVector, MaxTargetDistance);
                }

                randomVector.Z = 0;

                return randomVector;
            }
        }
        // Declared public member fields and properties will show in the game studio

        public override void Start()
        {
            random = new Random();

            velocity = Vector3.Multiply(randomVector, Speed);
            startPosition = Entity.Transform.Position;
            currentTarget = startPosition + randomVector;
            timeStarted = Game.UpdateTime.Total;
        }

        public override void Update()
        {
            var timeAmount = (Game.UpdateTime.Total - timeStarted).TotalSeconds / ChangeInterval;

            if (timeAmount > 1)
            {
                timeStarted = Game.UpdateTime.Total;
                currentTarget = startPosition + randomVector;
            }

            velocity += Vector3.Multiply(currentTarget - Entity.Transform.Position, Speed * (float) Game.UpdateTime.Elapsed.TotalSeconds);
            if (velocity.Length() > MaxVelocity)
            {
                velocity = Vector3.Multiply(Vector3.Normalize(velocity), MaxVelocity);
            }

            Entity.Transform.Position += Vector3.Multiply(velocity, (float) Game.UpdateTime.Elapsed.TotalSeconds);
        }
    }
}
