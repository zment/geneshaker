﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xenko.Core.Mathematics;
using Xenko.Input;
using Xenko.Engine;
using Xenko.Core;

namespace GeneShaker
{
    public class BasePair : SyncScript
    {
        public enum NitrogenBaseEnum
        {
            Guanine,
            Adenine,
            Cytosine,
            Thymine
        }
        
        public enum ConnectionSizeEnum
        {
            Two,
            Three
        }

        [DataMemberIgnore]
        public NitrogenBaseEnum[] NitrogenBaseEnumMap = { NitrogenBaseEnum.Cytosine, NitrogenBaseEnum.Thymine, NitrogenBaseEnum.Guanine, NitrogenBaseEnum.Adenine };
        private NitrogenBaseEnum _baseA;
        private NitrogenBaseEnum _baseB;
        public NitrogenBaseEnum BaseA {
            get { return _baseA; }
            set {
                _baseA = value;
                _baseB = NitrogenBaseEnumMap[(int) _baseA];
                BaseEntityA?.SetBase(_baseA);
                BaseEntityB?.SetBase(_baseB);
            }
        }
        public NitrogenBaseEnum BaseB {
            get { return _baseB; } 
            set {
                BaseA = NitrogenBaseEnumMap[(int)value];
            } 
        }

        public ConnectionSizeEnum ConnectionSize =>
            (BaseA == NitrogenBaseEnum.Guanine || BaseA == NitrogenBaseEnum.Cytosine) ?
            ConnectionSizeEnum.Three :
            ConnectionSizeEnum.Two;

        public NitrogenBase BaseEntityA;
        public NitrogenBase BaseEntityB;

        public override void Start()
        {
            // Initialization of the script.
        }

        public override void Update()
        {
            // Do stuff every new frame
        }
    }
}
