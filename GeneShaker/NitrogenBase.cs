﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xenko.Core.Mathematics;
using Xenko.Input;
using Xenko.Engine;

namespace GeneShaker
{
    public class NitrogenBase : SyncScript
    {
        public List<Entity> BaseModels = new List<Entity>();

        private BasePair.NitrogenBaseEnum _base;
        public BasePair.NitrogenBaseEnum Base {
            get { return _base; }
            set {
                if (BaseModels.Count() > 0)
                {
                    BaseModels.ForEach((x) => x.EnableAll(false, true));
                    BaseModels[(int)value].EnableAll(true, true);
                }

                _base = value;
            }
        }

        public void SetBase(BasePair.NitrogenBaseEnum newBase)
        {
            Base = newBase;
        }

        public override void Start()
        {
            // Initialization of the script.
        }

        public override void Update()
        {
            // Do stuff every new frame
        }
    }
}
