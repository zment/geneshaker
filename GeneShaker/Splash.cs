﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xenko.Core.Mathematics;
using Xenko.Input;
using Xenko.Engine;

namespace GeneShaker
{
    public class Splash : SyncScript
    {
        // Declared public member fields and properties will show in the game studio
        public float TimeToShow = 5f;
        public Scene NextScene;

        private TimeSpan startTime;
        public override void Start()
        {
            startTime = Game.UpdateTime.Total;
        }

        public override void Update()
        {
            if ((Game.UpdateTime.Total - startTime).TotalSeconds > TimeToShow)
            {
                SceneSystem.SceneInstance.RootScene = NextScene;
            }
        }
    }
}
