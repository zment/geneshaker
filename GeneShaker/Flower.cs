﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xenko.Core.Mathematics;
using Xenko.Input;
using Xenko.Engine;

namespace GeneShaker
{
    public class Flower : SyncScript
    {
        public Prefab FlowerSegmentPrefab;
        public Entity FlowerPetals;
        public int SegmentAmount = 4;

        private List<FlowerSegment> _flowerSegments = new List<FlowerSegment>();

        private static Random _random;
        public override void Start()
        {
            _random = new Random();

            for (int i = 0; i < SegmentAmount; i++)
            {
                var flowerSegmentEntity = FlowerSegmentPrefab.Instantiate().First();
                var flowerSegment = flowerSegmentEntity.Get<FlowerSegment>();
                flowerSegmentEntity.Transform.Parent = Entity.Transform;
                flowerSegment.Flower = this;

                if (i > 0)
                {
                    var lastFlowerSegment = _flowerSegments.Last();
                    flowerSegment.ParentSegment = lastFlowerSegment;
                    lastFlowerSegment.ChildSegment = flowerSegment;
                }

                flowerSegment.AngleIndex = _random.Next(4);
                flowerSegment.LengthIndex = _random.Next(4);
                _flowerSegments.Add(flowerSegment);
            }
        }

        public FlowerSegment this[int index]
        {
            get
            {
                if (_flowerSegments.Count > index)
                {
                    return _flowerSegments[index];
                }

                return null;
            }
        }

        public override void Update()
        {
            // Do stuff every new frame
        }

        public void UpdateFlowerPetals()
        {
            if (FlowerPetals != null)
            {
                var lastSegment = this[SegmentAmount - 1];
                FlowerPetals.Transform.Position = lastSegment.TipPosition;
                FlowerPetals.Transform.Rotation = Quaternion.RotationZ((float)(lastSegment.WorldAngle / 360 * Math.PI * 2));
            }
        }
    }
}
