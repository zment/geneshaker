﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xenko.Core.Mathematics;
using Xenko.Input;
using Xenko.Engine;
using Xenko.Games;
using Xenko.Audio;

namespace GeneShaker
{
    public class FlowerStrandBridge : SyncScript
    {
        public Flower Flower;
        public Strand Strand;
        public Sound Music;

        public override void Start()
        {
            var soundInstance = Music.CreateInstance();
            Audio.AudioEngine.MasterVolume = 0.1f;
            soundInstance.IsLooping = true;
            soundInstance.Volume = 0.05f;
            soundInstance.Play();

            if (Strand == null || Flower == null)
                return;

            // Initially change the strand to reflect the flower
            for (int i = 0; i < Strand.Amount; i++)
            {
                var flower = Flower[i / 2];
                if (i % 2 == 0)
                    Strand[i] = flower.AngleIndex;
                else
                    Strand[i] = flower.LengthIndex;
            }

            Flower.UpdateFlowerPetals();
            Strand.Changed += Strand_Changed;
        }

        private void Strand_Changed(Strand strand, int index)
        {
            var flower = Flower[index / 2];
            if (index % 2 == 0)
                flower.AngleIndex = strand[index];
            else
                flower.LengthIndex = strand[index];
        }

        private readonly Keys[] keys = { Keys.D1, Keys.D2, Keys.D3, Keys.D4, Keys.D5, Keys.D6, Keys.D7, Keys.D8 };
        public override void Update()
        {
            if (!Game.IsRunning)
                return;

            for (int i = 0; i < keys.Length; i++)
            {
                if (Input.IsKeyPressed(keys[i]))
                {
                    var strandValue = Strand[i];
                    Strand[i] = (strandValue + 1) % 4;
                }
            }

            if (Input.IsKeyPressed(Keys.Escape))
            {
                (Game as GameBase).Exit();
            }
        }
    }
}
