﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xenko.Core.Mathematics;
using Xenko.Input;
using Xenko.Engine;
using Xenko.Graphics.GeometricPrimitives;
using Xenko.Graphics;
using Xenko.Core;

namespace GeneShaker
{
    public class FlowerSegment : SyncScript
    {
        [DataMemberIgnore]
        public readonly float[] Angles = { -22.5f, -11.25f, 11.25f, 22.5f };
        [DataMemberIgnore]
        public readonly float[] Lengths = { 0.5f, 0.75f, 1.25f, 1.5f };

        public Entity BaseEntity;
        public Entity SegmentTip;

        [DataMemberIgnore]
        public Vector3 TipPosition => BaseEntity.Transform.Position + _segmentVector;
        private Vector3 _segmentVector;

        [DataMemberIgnore]
        public FlowerSegment ParentSegment;
        [DataMemberIgnore]
        public FlowerSegment ChildSegment;
        [DataMemberIgnore]
        public Flower Flower;

        [DataMemberIgnore]
        public Vector3 SegmentDirection => Vector3.Normalize(_segmentVector);

        private int _lengthIndex = 0;
        private int _angleIndex = 0;

        public int AngleIndex
        {
            get { return _angleIndex; }
            set
            {
                _angleIndex = Math.Max(Math.Min(Angles.Length - 1, value), 0);
                UpdateSegment();
            }
        }
        public int LengthIndex
        {
            get { return _lengthIndex; }
            set
            {
                _lengthIndex = Math.Max(Math.Min(Angles.Length - 1, value), 0);
                UpdateSegment();
            }
        }

        public float Angle => Angles[AngleIndex];
        public float WorldAngle => _currentAngle + (ParentSegment != null ? ParentSegment.WorldAngle : 0);
        public float Length => Lengths[LengthIndex];
        public float WorldLength => _currentLength;

        private float _currentAngle;
        private float _currentLength;

        public override void Start()
        {
            // Initialization of the script.
        }

        public override void Update()
        {
            if (_currentAngle != Angle || _currentLength != Length)
            {
                _currentAngle = (float) MathUtil.Lerp(_currentAngle, Angle, Game.UpdateTime.Elapsed.TotalSeconds);
                _currentLength = (float)MathUtil.Lerp(_currentLength, Length, Game.UpdateTime.Elapsed.TotalSeconds);
                UpdateSegment();
            }
        }

        public void UpdateSegment()
        {
            if (ParentSegment != null)
                BaseEntity.Transform.Position = ParentSegment.TipPosition;

            BaseEntity.Transform.Scale.Y = WorldLength;

            var angle = (float)((WorldAngle) / 360 * (Math.PI * 2));
            var rotation = Quaternion.RotationZ(angle);
            BaseEntity.Transform.Rotation = rotation;

            var directionVector = new Vector3(0, 1, 0);
            rotation.Rotate(ref directionVector);

            _segmentVector = Vector3.Multiply(directionVector, WorldLength);

            SegmentTip.Transform.Position = TipPosition;

            ChildSegment?.UpdateSegment();

            if (Flower != null && ChildSegment != null)
            {
                Flower.UpdateFlowerPetals();
            }
        }
    }
}
