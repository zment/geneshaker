﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xenko.Core.Mathematics;
using Xenko.Input;
using Xenko.Engine;

namespace GeneShaker
{
    // Strand consists of multiple DNA Basepairs. They are created on the fly according to the settings given.
    public class Strand : SyncScript
    {
        public Prefab BasePairPrefab;
        public int Amount;
        public float TwistAngle;
        private List<BasePair> BasePairs = new List<BasePair>();
        private static Random random;

        public delegate void OnChangedEventHandler(Strand strand, int index);
        public event OnChangedEventHandler Changed;

        public int this[int index]
        {
            get
            {
                if (BasePairs.Count > index)
                {
                    return (int) BasePairs[index].BaseA;
                }

                return -1;
            }

            set
            {
                if (BasePairs.Count > index)
                {
                    BasePairs[index].BaseA = (BasePair.NitrogenBaseEnum) value;

                    Changed?.Invoke(this, index);
                }
            }
        }

        public override void Start()
        {
            random = new Random();

            for (int i = 0; i < Amount; i++)
            {
                var basePairInstance = BasePairPrefab.Instantiate().First();
                var basePair = basePairInstance.Get<BasePair>();
                BasePairs.Add(basePair);

                basePairInstance.Transform.Parent = Entity.Transform;
                basePairInstance.Transform.Position.Y = i - (Amount / 2 - (Amount % 2 / 2)) + 0.5f;
                basePairInstance.Transform.RotationEulerXYZ = new Vector3(0, (float) ((i * TwistAngle / 180f) * Math.PI), 0);

                basePair.BaseA = (BasePair.NitrogenBaseEnum) random.Next(4);
            }
        }

        public override void Update()
        {
            // Do stuff every new frame
        }
    }
}
