﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xenko.Core.Mathematics;
using Xenko.Input;
using Xenko.Engine;

namespace GeneShaker
{
    public class Rotate : SyncScript
    {
        // Declared public member fields and properties will show in the game studio
        public Vector3 RotationVector;
        public float Speed = 1f;

        private Vector3 currentEulerRotation;
        private const float TAU = (float) (Math.PI*2);

        public override void Start()
        {
            currentEulerRotation = Entity.Transform.RotationEulerXYZ;
        }

        public override void Update()
        {
            var newEulerRotation = currentEulerRotation + Vector3.Multiply(RotationVector, (float)Game.UpdateTime.Elapsed.TotalSeconds * Speed);
            newEulerRotation.X %= TAU;
            newEulerRotation.Y %= TAU;
            newEulerRotation.Z %= TAU;

            Entity.Transform.RotationEulerXYZ = currentEulerRotation = newEulerRotation;
        }
    }
}
