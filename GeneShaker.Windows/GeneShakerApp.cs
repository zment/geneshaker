using Xenko.Engine;

namespace GeneShaker.Windows
{
    class GeneShakerApp
    {
        static void Main(string[] args)
        {
            using (var game = new Game())
            {
//                game.GraphicsDeviceManager.IsFullScreen = true;
//                game.GraphicsDeviceManager.ApplyChanges();

                game.Run();
            }
        }
    }
}
